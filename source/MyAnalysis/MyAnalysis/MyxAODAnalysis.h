#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <TH1.h>
#include <TTree.h>

#include <EventLoop/Algorithm.h>
// GRL
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
#include <AsgTools/AnaToolHandle.h>

class MyxAODAnalysis : public EL::Algorithm
{
  asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //!
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
 public:
  // float cutValue;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
 public:
  std::string outputName;
  TTree *tree; //!
  int m_numCleanEvents = 0; //!
  TH1 *h_jetPt; //!
  int EventNumber; //!
  int nJets; //!
  int nMuons; //!
  int nElectrons; //!

  // this is a standard constructor
  MyxAODAnalysis ();
  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MyxAODAnalysis, 1);
};

#endif
