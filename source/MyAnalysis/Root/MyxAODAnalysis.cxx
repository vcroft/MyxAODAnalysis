#include <xAODJet/JetContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODCore/AuxContainerBase.h>
#include <xAODCore/ShallowCopy.h>
#include <xAODEventInfo/EventInfo.h>
#include <AsgTools/MessageCheck.h>
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <TSystem.h>
#include <TFile.h>

// this is needed to distribute the algorithm to the workers
ClassImp(MyxAODAnalysis)



MyxAODAnalysis :: MyxAODAnalysis ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  
}

EL::StatusCode MyxAODAnalysis :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  // let's initialize the algorithm to use the xAODRootAccess package
  job.useXAOD ();

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysis :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  //  book (TH1F ("h_jetPt", "h_jetPt", 100, 0, 500)); // jet pt [GeV]
  h_jetPt = new TH1F("h_jetPt", "h_jetPt", 100, 0, 500); // jet pt [GeV]
  wk()->addOutput (h_jetPt);

  // get the output file, create a new TTree and connect it to that output
  // define what braches will go in that tree
  TFile *outputFile = wk()->getOutputFile (outputName);
  tree = new TTree ("tree", "tree");
  tree->SetDirectory (outputFile);
  tree->Branch ("EventNumber", &EventNumber);
  tree->Branch ("nJets", &nJets);
  tree->Branch ("nMuons", &nMuons);
  tree->Branch ("nElectrons", &nElectrons);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: changeInput (bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  ANA_MSG_INFO ("in initialize");
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // set type of return code you are expecting
  // (add to top of each function once)
  ANA_CHECK_SET_TYPE (EL::StatusCode);

  //----------------------------
  // Event information
  //--------------------------- 
  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(evtStore()->retrieve( eventInfo, "EventInfo"));  
  EventNumber = eventInfo->eventNumber();
  // check if the event is data or MC
  // (many tools are applied either to data or MC)
  bool isMC = false;
  // check if the event is MC
  if (eventInfo->eventType (xAOD::EventInfo::IS_SIMULATION)) {
    isMC = true; // can do something with this later
  }

  // if data check if event passes GRL
  if (!isMC) { // it's data!
    if (!m_grl->passRunLB(*eventInfo)) {
      ANA_MSG_INFO ("drop event: GRL");
      return EL::StatusCode::SUCCESS; // go to next event
    }
  } // end if not MC

  ANA_MSG_INFO ("keep event: GRL");


  const xAOD::JetContainer* jets = 0;
  ANA_CHECK(evtStore()->retrieve( jets, "AntiKt4EMTopoJets" ));

  //--------------
  // shallow copy 
  //--------------
  // "jets" jet container already defined above
  auto shallowCopy = xAOD::shallowCopyContainer (*jets);
  std::unique_ptr<xAOD::JetContainer> shallowJets (shallowCopy.first);
  std::unique_ptr<xAOD::ShallowAuxContainer> shallowAux (shallowCopy.second);

  // iterate over the shallow copy
  for (auto jetSC : *shallowCopy.first) {
    // apply a shift in pt, up by 5%
    double newPt =  jetSC->pt() * (1 + 0.05);

    // from: https://gitlab.cern.ch/atlas/athena/tree/21.2/Event/xAOD/xAODJet/xAODJet/versions/Jet_v1.h
    xAOD::JetFourMom_t newp4 (newPt, jetSC->eta(), jetSC->phi(), jetSC->m());
    jetSC->setJetP4 (newp4); // we've overwritten the 4-momentum
  } // end iterator over jet shallow copy
  int numberofjets = 0;
  // loop over the jets in the container
  for (auto jet : *jets) {
    if(jet->pt() * 0.001 > 50.){
      numberofjets++;
    }
    hist("h_jetPt")->Fill (jet->pt() * 0.001 ); //GeV
    ANA_MSG_INFO ("execute(): jet pt = " << (jet->pt() * 0.001) << " GeV"); // just to print out something
    if (jet->pt() > 40000) {
      jet->auxdecor<int>("mySignal") = 1; // 1 = yes, it's a signal jet!
    } else {
      jet->auxdecor<int>("mySignal") = 0; // 0 = nope, not a signal jet
    }
  } // end for loop over jets
  nJets = numberofjets;
  // iterate over the shallow copy
  for (auto jetSC : *shallowCopy.first) {
    // adding a (silly) variable: checking if shallow copied pt is greater than 40 GeV, after 5% shift up (classify as signal or not)
    if (jetSC->pt() > 40000) {
      jetSC->auxdata<int>("mySignal") = 1; // 1 = yes, it's a signal jet!
    } else {
      jetSC->auxdata<int>("mySignal") = 0; // 0 = nope, not a signal jet
    }
  } // end iterator over jet shallow copy

  //------------
  // MUONS
  //------------
  // get muon container of interest
  const xAOD::MuonContainer* muons = 0;
  ANA_CHECK (evtStore()->retrieve (muons, "Muons"));
  int numberofmuons = 0;
  // loop over the muons in the container
  for (auto muon : *muons) {
    if(muon->pt() * 0.001 > 10.){
      numberofmuons++;
    }
    ANA_MSG_INFO ("execute(): original muon pt = " << ((muon)->pt() * 0.001) << " GeV"); // just to print out something
  } // end for loop over muons
  nMuons = numberofmuons;

  //------------
  // Electrons
  //------------
  // get electron container of interest
  const xAOD::ElectronContainer* electrons = 0;
  ANA_CHECK (evtStore()->retrieve (electrons, "Electrons"));
  int numberofelectrons = 0;
  // loop over the muons in the container
  for (auto electron : *electrons) {
    if(electron->pt() * 0.001 > 15.){
      numberofelectrons++;
    }
    ANA_MSG_INFO ("execute(): original electron pt = " << ((electron)->pt() * 0.001) << " GeV"); // just to print out something
  } // end for loop over muons
  nElectrons = numberofelectrons;



  tree->Fill();
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode MyxAODAnalysis :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
