# AnalysisBase Tutorial code
based on the tutorial given [here](https://atlassoftwaredocs.web.cern.ch/ABtutorial/) for the 10-14July 2017 ATLAS offline tutorial.

This code takes in an example DxAOD found at the following location:
```
export ALRB_TutorialData=/afs/cern.ch/atlas/project/PAT/tutorial/cern-july2017/
```

This is a simple PowhegPythia8 truth unfiltered ttbar sample with 500 Events.

## Goal
This code extracts information from xAODEventInfo::EventInfo xAODEgamma::ElectronContainer xAODMuon::MuonContainer xAODJet::JetContainer to produce an output file containing a single TTree called tree. This contains branches for the EventNumber nJets nElectrons and nMuons.

## Layout
The code contains three folders. Run, Build, and Source. Only the source folder contains nice useful code. 

## Building
in the build dir 
```
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh 
asetup 21.2.0,AnalysisBase
source /afs/cern.ch/user/v/vcroft/ROOTAnalysisTutorial/build/x86_64-slc6-gcc62-opt/setup.sh 
cmake ../source/
make
```

## Running

source/MyAnalysis/util/ATestRun.cxx is a compiled macro used to produce an application called ATestRun. I like to run from the `run` directory since that's what it's there for.  

To run a command
```
ATestRun someFolderName
```
this output folder must not already exist because it is created and contains a folder called `data-myOutput` with the produced rootfile as an output. 
