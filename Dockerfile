#
# Image providing the project on top of an AnalysisBase image.
#

# Set up the base image
FROM atlas/analysisbase:21.2.4

# Copy the projects sources into the image:
COPY source/ source

# Build the project inside a build/ directory:
RUN source /home/atlas/release_setup.sh && \
    mkdir build && cd build/ && cmake ../source/ && cmake --build . && cpack

